FROM debian:9 as build

ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1

RUN apt-get update && apt-get install -y git wget gcc make libnginx-mod-http-lua libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
WORKDIR /usr/local/
RUN wget 'https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz' && tar -xzvf v0.3.1.tar.gz 
RUN wget 'https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.20.tar.gz' && tar -xzvf v0.10.20.tar.gz
RUN git clone https://github.com/openresty/luajit2.git && cd luajit2/ && make
RUN ls /usr/local/include/luajit-2.1
RUN ls /usr/local/lib

RUN wget 'https://openresty.org/download/nginx-1.19.3.tar.gz' && tar -xzvf nginx-1.19.3.tar.gz && cd nginx-1.19.3/ && ./configure --prefix=/usr/local/nginx --with-ld-opt="-Wl,-rpath,/usr/local/lib"  --add-module=/usr/local/ngx_devel_kit-0.3.1 --add-module=/usr/local/lua-nginx-module-0.10.20  && make && make install

FROM debian:9
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
